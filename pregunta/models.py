from django.db import models
from django.db.models.fields import CharField


# Create your models here.

class PreguntaUnica(models.Model):
    texto=models.CharField(max_length=500)
    
    def __str__(self):
        return self.texto

# desea contestar encuentas




class PreguntasOpcion(models.Model):
    preguntas=models.ForeignKey(PreguntaUnica, related_name='cargo', on_delete=models.CASCADE)
    opcion = models.CharField(max_length=200) # Campo tipo Char con un máximo de 200 caracteres de longitud.
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.preguntas
