from django import forms
from django.shortcuts import render
from pregunta.forms import UnicaPregunta , Multipregunta


# Create your views here.
def preguntaBase(request):
 
    return render(request,"pregunta/preguntaBase.html")

def pregunta(request):
    preguntas=UnicaPregunta
    data={
         'form': preguntas
    }
    if request.method == 'POST':
        unica=UnicaPregunta(data=request.POST)
        if unica.is_valid():
            unica.save()
            return render(request,"formulario/formulario.html")
        else:
            data["form"]=unica
    return render(request,"pregunta/pregunta.html",data)

def preguntaMulti(request):
    preguntas=Multipregunta
    data={
         'form': preguntas
    }
    if request.method == 'POST':
        unica=Multipregunta(data=request.POST)
        if unica.is_valid():
            unica.save()
            return render(request,"formulario/formulario.html")
        else:
            data["form"]=unica
    return render(request,"pregunta/preguntasMulti.html",data)



































# def countries_view(request):
#     paises=CountryForm
#     data={
#         'form':paises 
#     }
#     if request.method == 'POST':
#         form = CountryForm(data=request.POST)
#         if form.is_valid():
#             form.save()
#             return render(request,"formulario/formulario.html")
#         else:
#             data["form"]=form
#     else:
#         form = CountryForm

#     return render(request,"pregunta/preguntasMulti.html",data )

