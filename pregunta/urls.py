from django.urls import path
from . import views

urlpatterns = [
    path('preguntaBase/',views.preguntaBase, name="preguntaBase"),
    path('pregunta/',views.pregunta, name="pregunta"),
    path('preguntasmulti/',views.preguntaMulti, name="preguntasmulti"),

]